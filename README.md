# Assessment Project - HTML, CSS, AngularJS

Welcome to the Booxware Front-End Assessment Project - HTML, CSS, AngularJS

Here you find a short summary of your tasks. Please use this project "fe_assessment" as your skeleton and add new sources to complete the tasks. 
Follow the **Development and Submission steps** in order to do the below **Tasks** and submit them to us for review.

## Tasks

Create a new web page where users are able to:

1. View bets as shown in the image FrontDevTest-closed.png
2. Betslip will by default be empty - no events
3. Odds are to be static (no-backend functionality) and clickable, being added to the betslip once added
4. If only one event is added to the betslip, SINGLE tab should be selected, as per FrontDevTest-open.png
5. In case of 2 or more events are added, COMBI tab is then selected
6. One cannot add events from the same group + game (eg: Laos - Vietnam, one can't bet on 1 and 2 for match odds. 
7. Proper client-side error handling + validations
8. Ability to delete bets from the betslip.
9. 'Place Bet' will display a message that the betslip has been submitted


## Development and Submission steps

1. Clone the master branch to your local machine
2. Please commit frequently to your local git repository (no need to push)
3. When ready, make sure everything is committed
4. Add any assumptions or notes to the file named COMMENTS.md
5. Compress your solution in ZIP format, upload to a cloud service and send us the link for us to be able to download the solution.


**Good luck!**